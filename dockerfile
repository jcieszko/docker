#Stworzenie pliku jar w kontenerze z gotowa aplikacja
FROM gradle:jdk17-jammy AS build
USER root
RUN mkdir app
COPY src/ /app/src/
COPY build.gradle /app/
COPY settings.gradle /app/
WORKDIR /app
RUN gradle --no-daemon clean bootJar

#uruchomienie aplikacji znajdujacej sie w pliku jar w kontenerze
#to copy jest w celu ulatwienia polecenia uruchomienia
FROM openjdk:17-ea-jdk
COPY --from=build /app/build/libs/lab3-0.0.1-SNAPSHOT.jar lab3-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/lab3-0.0.1-SNAPSHOT.jar"]